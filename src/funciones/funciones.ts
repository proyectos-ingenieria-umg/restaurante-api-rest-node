import { TokenInfoModel, UsuarioModel } from '../modelos/modelos';
import { Request } from 'express';

export function crearToken(usuario: UsuarioModel): string {
  const claims: TokenInfoModel = {
    usuario_id: usuario.id,
    email: usuario.usuario_email,
    nombre: usuario.usuario_nombre,
    rol: 'CAJERO',
    createdAt: new Date().getTime(),
  };

  return Buffer.from(JSON.stringify(claims)).toString('base64');
}

export function validarToken(authorization: string): TokenInfoModel {
  if (!authorization) {
    return null;
  }

  const headerParts = authorization.split(' ');

  if (headerParts.length <= 1 || headerParts[0] !== 'Basic') {
    return null;
  }

  const token = headerParts[1];

  const claims = Buffer.from(token, 'base64').toString('ascii');

  return JSON.parse(claims) as TokenInfoModel;
}