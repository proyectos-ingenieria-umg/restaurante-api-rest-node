import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { ProductoModel } from '../modelos/modelos';

@Injectable()
export class ProductoRepositorio {

  constructor(private readonly connection: Connection) {
  }

  async crearProducto(producto: ProductoModel): Promise<number> {
    const queryRunner = this.connection.createQueryRunner();

    let id = 0;

    try {
      const query = `
      insert into producto(activo, creado_por, fecha_creacion, precio, producto_descripcion, producto_nombre, producto_tipo_id, categoria_id)
      values(true, $1, current_timestamp, $2, $3, $4, $5, $6)
      `;

      const data = [
        producto.creado_por,
        producto.precio,
        producto.producto_descripcion,
        producto.producto_nombre,
        producto.producto_tipo_id,
        producto.categoria_id,
      ];

      const resultado = await queryRunner.query(query, data);

      id = resultado[0].id;
    } catch (e) {
      console.error(e);
    } finally {
      queryRunner.release();
    }

    return id;
  }

  async actualizarProducto(producto: ProductoModel): Promise<void> {
    const queryRunner = this.connection.createQueryRunner();

    try {
      const query = `
      update producto
        set precio = $1, 
            producto_descripcion = $2, 
            producto_nombre = $3,
            producto_tipo_id = $4,
            categoria_id = $5,
            activo = $6
      where id = $7
      `;

      const data = [
        producto.precio,
        producto.producto_descripcion,
        producto.producto_nombre,
        producto.producto_tipo_id,
        producto.categoria_id,
        producto.activo,
        producto.id,
      ];

      await queryRunner.query(query, data);
    } catch (e) {
      console.error(e);
    } finally {
      queryRunner.release();
    }
  }

  async obtenerProductosPorCategoria(categoriaId: number): Promise<ProductoModel[]> {
    const queryRunner = this.connection.createQueryRunner();

    let productos: ProductoModel[] = [];

    try {
      const query = `
    select t0.id, 
           t0.activo, 
           t0.creado_por, 
           t0.fecha_creacion, 
           t0.precio, 
           t0.producto_descripcion, 
           t0.producto_nombre, 
           t0.producto_tipo_id, 
           t0.categoria_id
    from producto as t0
      join categoria as t1 on t1.id = t0.categoria_id
      join producto_tipo as t2 on t2.id = t0.producto_tipo_id
    where t0.activo = true
      and t0.categoria_id = ${categoriaId}
    `;

      productos = await queryRunner.query(query);
    } catch (e) {
      console.error(e);
    } finally {
      queryRunner.release();
    }

    return productos;
  }

}

