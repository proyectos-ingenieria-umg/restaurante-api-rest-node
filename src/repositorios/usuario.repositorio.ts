import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { UsuarioModel } from '../modelos/modelos';

@Injectable()
export class UsuarioRepositorio {

  constructor(private readonly connection: Connection) {

  }

  async buscarUsuarioPorEmail(email: string): Promise<UsuarioModel[]> {
    const queryRunner = this.connection.createQueryRunner();

    let usuarios: UsuarioModel[] = [];

    try {
      const query = `
      select id,
        activo,
        usuario_email, 
        usuario_nombre,
        usuario_password,
        fecha_creacion,
        creado_por
      from usuario
       where usuario_email = '${email}'
      `;

      usuarios = await queryRunner.query(query);
    } catch (e) {
      console.error(e);
    } finally {
      queryRunner.release();
    }

    return usuarios;
  }

}
