import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { MesaModel } from '../modelos/modelos';

@Injectable()
export class MesaRepositorio {

  constructor(private readonly connection: Connection) {
  }

  /**
   * Realiza el insert en la tabla mesa de la base de datos
   * @param mesa datos de la mesa
   * @return Retorna promesa con el ID de la mesa insertada
   */
  async crearMesa(mesa: MesaModel): Promise<number> {
    const queryRunner = this.connection.createQueryRunner();

    let id = 0;

    try {

      const query = `
      insert into mesa(activo, creado_por, fecha_creacion, cantidad_maxima_personas, numero_mesa, mesa_estado_id)
      values(true, $1, current_timestamp, $2, $3, $4) returning id
      `;

      const datos = [mesa.creado_por, mesa.cantidad_maxima_personas, mesa.numero_mesa, mesa.mesa_estado_id];
      const resultado = await queryRunner.query(query, datos);

      id = resultado[0].id;
    } catch (e) {
      console.error(e);
    } finally {
      queryRunner.release();
    }

    return id;
  }

  /**
   * Hace el update de una mesa
   * @param mesa datos de la mesa que se va a actualizar
   */
  async actualizarMesa(mesa: MesaModel): Promise<void> {
    const queryRunner = this.connection.createQueryRunner();

    try {
      const query = `
      update mesa
        set activo = $1, cantidad_maxima_personas = $2, numero_mesa = $3, mesa_estado_id = $4
       where id = $5
      `;

      const datos = [mesa.activo, mesa.cantidad_maxima_personas, mesa.numero_mesa, mesa.mesa_estado_id, mesa.id];

      await queryRunner.query(query, datos);
    } catch (e) {
      console.error(e);
    } finally {
      queryRunner.release();
    }
  }

  /**
   * Obtiene el listado de mesas de la tabla mesa
   * @return Retorna promesa con el listado de mesas
   */
  async obtenerMesas(): Promise<MesaModel[]> {
    const queryRunner = this.connection.createQueryRunner();

    let mesas: MesaModel[] = [];

    try {
      const query = `
      select t0.id, t0.creado_por, t0.fecha_creacion, t0.cantidad_maxima_personas, t0.numero_mesa, t0.mesa_estado_id, t1.mesa_estado_nombre
      from mesa as t0
      join mesa_estado as t1 on t1.id = t0.mesa_estado_id
      where t0.activo = true
      `;

      mesas = await queryRunner.query(query);
    } catch (e) {
      console.error(e);
    } finally {
      queryRunner.release();
    }

    return mesas;
  }

}
