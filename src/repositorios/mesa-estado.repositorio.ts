import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { MesaEstadoModel } from '../modelos/modelos';

@Injectable()
export class MesaEstadoRepositorio {

  constructor(private readonly connection: Connection) {
  }

  async obtenerEstadoPorNombre(nombreEstado: string): Promise<MesaEstadoModel> {
    const queryRunner = this.connection.createQueryRunner();

    let estado: MesaEstadoModel = null;

    try {
      const query = `
      select id, mesa_estado_nombre, fecha_creacion, creado_por, activo
      from mesa_estado
      where mesa_estado_nombre = ${nombreEstado}
      `;

      const resultado: MesaEstadoModel[] = await queryRunner.query(query);

      if (resultado.length > 0) {
        estado = resultado[0];
      }

    } catch (e) {
      console.error(e);
    } finally {
      queryRunner.release();
    }

    return estado;

  }

}
