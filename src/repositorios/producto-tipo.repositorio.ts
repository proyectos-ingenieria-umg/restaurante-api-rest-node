import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { ProductoTipoModel } from '../modelos/modelos';

@Injectable()
export class ProductoTipoRepositorio {

  constructor(private readonly connection: Connection) {

  }

  async obtenerProductoTipos(): Promise<ProductoTipoModel[]> {
    const queryRunner = this.connection.createQueryRunner();

    let tipos: ProductoTipoModel[] = [];

    try {
      const query = `
      select id, activo, creado_por, fecha_creacion, producto_tipo_nombre
      from producto_tipo
      where activo = true
      order by producto_tipo_nombre asc;
      `;

      tipos = await queryRunner.query(query);
    } catch (e) {
      console.error(e);
    }

    return tipos;
  }

}
