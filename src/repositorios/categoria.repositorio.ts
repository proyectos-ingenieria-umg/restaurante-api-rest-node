import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { CategoriaModel } from '../modelos/modelos';

@Injectable()
export class CategoriaRepositorio {

  constructor(private readonly connection: Connection) {
  }

  async crearCategoria(categoria: CategoriaModel): Promise<number> {
    const queryRunner = this.connection.createQueryRunner();

    let id = 0;

    try {
      const query = `
      insert into categoria(activo, creado_por, fecha_creacion, categoria_nombre) 
      values(true, $1, current_timestamp, $2) returning id;
      `;

      const data = [categoria.creado_por, categoria.categoria_nombre];

      const resultado = await queryRunner.query(query, data);

      id = resultado[0].id;
    } catch (e) {
      console.error(e);
    } finally {
      queryRunner.release();
    }

    return id;
  }

  async actualizarCategoria(categoria: CategoriaModel): Promise<void> {
    const queryRunner = this.connection.createQueryRunner();

    try {
      const query = `
      update categoria set '${categoria.categoria_nombre}' where id = ${categoria.id}
      `;

      await queryRunner.query(query);
    } catch (e) {
      console.error(e);
    } finally {
      queryRunner.release();
    }
  }

  async obtenerCategorias(): Promise<CategoriaModel[]> {
    const queryRunner = this.connection.createQueryRunner();

    let categorias: CategoriaModel[] = [];

    try {
      const query = `
      select id, creado_por, fecha_creacion, categoria_nombre
      from categoria
      where activo = true
      order by categoria_nombre asc;
      `;

      categorias = await queryRunner.query(query);
    } catch (e) {
      console.error(e);
    } finally {
      queryRunner.release();
    }

    return categorias;
  }

}
