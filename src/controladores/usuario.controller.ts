import { Controller, HttpCode, Post, Body } from '@nestjs/common';
import { LoginRespuesta, LoginSolicitud, UsuarioModel } from '../modelos/modelos';
import { UsuarioRepositorio } from '../repositorios/usuario.repositorio';
import { crearToken } from '../funciones/funciones';

@Controller('api/usuario')
export class UsuarioController {

  constructor(private readonly usuarioConsultas: UsuarioRepositorio) {
  }

  @Post('/login')
  @HttpCode(200)
  async login(@Body() solicitud: LoginSolicitud): Promise<LoginRespuesta> {

    const respuesta: LoginRespuesta = {
      loginExitoso: false,
    };

    const email = solicitud.email;
    const password = solicitud.password;

    // 1. Verificamos los datos requeridos
    if (!email || !password) {
      respuesta.error = 'Usuario y password son requeridos';
      return respuesta;
    }

    // 2. Buscamos el usuario por su email
    const listaUsuarios: UsuarioModel[] = await this.usuarioConsultas.buscarUsuarioPorEmail(email);

    if (!listaUsuarios || listaUsuarios.length === 0) {
      respuesta.error = 'Usuario o password incorrectos';
      return respuesta;
    }

    // 3. Codificamos el password a base64 para compararlo con el password del usuario
    const passwordCodificado = Buffer.from(password).toString('base64');

    const usuario = listaUsuarios[0];

    // 4. Hacemos la comparacion de passwords
    // Si el password es correcto, realizamos login, creamos token y retornamos la respuesta
    // de lo contrario retornamos un error
    if (passwordCodificado == usuario.usuario_password) {
      respuesta.loginExitoso = true;
      respuesta.token = crearToken(usuario);
      return respuesta;
    } else {
      respuesta.error = 'Usuario o password incorrectos';
      return respuesta;
    }

  }

}
