import { Body, Controller, Get, Headers, Post, Put, Query, UnauthorizedException } from '@nestjs/common';
import { ProductoRepositorio } from '../repositorios/producto.repositorio';
import { ProductoModel } from '../modelos/modelos';
import { validarToken } from '../funciones/funciones';

@Controller('api/producto')
export class ProductoController {

  constructor(private readonly productoRepositorio: ProductoRepositorio) {
  }

  @Get()
  obtenerProductosPorCategoria(@Query('categoriaId') categoriaId: number): Promise<ProductoModel[]> {
    return this.productoRepositorio.obtenerProductosPorCategoria(categoriaId);
  }

  @Post()
  crearProducto(
    @Headers('authorization') authorization: string,
    @Body() producto: ProductoModel,
  ): Promise<number> {
    const tokenInfo = validarToken(authorization);

    if (!tokenInfo) {
      throw new UnauthorizedException('Acceso no autorizado');
    }

    producto.creado_por = tokenInfo.email;

    return this.productoRepositorio.crearProducto(producto);
  }

  @Put()
  actualizarProducto(
    @Headers('authorization') authorization: string,
    @Body() producto: ProductoModel,
  ): Promise<void> {
    const tokenInfo = validarToken(authorization);

    if (!tokenInfo) {
      throw new UnauthorizedException('Acceso no autorizado');
    }

    return this.productoRepositorio.actualizarProducto(producto);
  }
}
