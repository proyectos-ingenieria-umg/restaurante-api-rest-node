import { Controller, Get } from '@nestjs/common';
import { ProductoTipoRepositorio } from '../repositorios/producto-tipo.repositorio';
import { ProductoTipoModel } from '../modelos/modelos';

@Controller('api/producto-tipo')
export class ProductoTipoController {

  constructor(private readonly productoTipoRepositorio: ProductoTipoRepositorio) {
  }

  @Get()
  obtenerProductoTipos(): Promise<ProductoTipoModel[]> {
    return this.productoTipoRepositorio.obtenerProductoTipos();
  }

}
