import { Body, Controller, Get, Post, Headers, UnauthorizedException, Put } from '@nestjs/common';
import { MesaRepositorio } from '../repositorios/mesa.repositorio';
import { MesaModel } from '../modelos/modelos';
import { MesaEstadoRepositorio } from '../repositorios/mesa-estado.repositorio';
import { validarToken } from '../funciones/funciones';

@Controller('api/mesa')
export class MesaController {

  constructor(
    private readonly mesaRepositorio: MesaRepositorio,
    private readonly mesaEstadoRepositorio: MesaEstadoRepositorio,
  ) {
  }

  @Post()
  async crearMesa(
    @Headers('authorization') authorization: string,
    @Body() mesa: MesaModel,
  ): Promise<number> {
    const tokenInfo = validarToken(authorization);

    if (!tokenInfo) {
      throw new UnauthorizedException('Acceso no autorizado');
    }

    const mesaEstado = await this.mesaEstadoRepositorio.obtenerEstadoPorNombre('Libre');

    mesa.mesa_estado_id = mesaEstado.id;
    mesa.creado_por = tokenInfo.email;

    return await this.mesaRepositorio.crearMesa(mesa);
  }

  @Put()
  async actualizarMesa(
    @Headers('authorization') authorization: string,
    @Body() mesa: MesaModel,
  ): Promise<void> {
    const tokenInfo = validarToken(authorization);

    if (!tokenInfo) {
      throw new UnauthorizedException('Acceso no autorizado');
    }

    return await this.mesaRepositorio.actualizarMesa(mesa);
  }

  @Get()
  obtenerMesas(): Promise<MesaModel[]> {
    return this.mesaRepositorio.obtenerMesas();
  }

}
