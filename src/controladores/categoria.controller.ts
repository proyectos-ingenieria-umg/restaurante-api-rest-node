import { Controller, Get, Headers, Post, Body, UnauthorizedException, Put } from '@nestjs/common';
import { CategoriaRepositorio } from '../repositorios/categoria.repositorio';
import { CategoriaModel } from '../modelos/modelos';
import { validarToken } from '../funciones/funciones';

@Controller('api/categoria')
export class CategoriaController {

  constructor(private readonly categoriaRepositorio: CategoriaRepositorio) {
  }

  @Get()
  obtenerCategorias(): Promise<CategoriaModel[]> {
    return this.categoriaRepositorio.obtenerCategorias();
  }

  @Post()
  crearCategoria(
    @Headers('authorization') authorization: string,
    @Body() categoria: CategoriaModel,
  ): Promise<number> {
    const tokenInfo = validarToken(authorization);

    if (!tokenInfo) {
      throw new UnauthorizedException('Acceso no autorizado');
    }

    categoria.creado_por = tokenInfo.email;

    return this.categoriaRepositorio.crearCategoria(categoria);
  }

  @Put()
  actualizarCategoria(
    @Headers('authorization') authorization: string,
    @Body() categoria: CategoriaModel,
  ): Promise<void> {
    const tokenInfo = validarToken(authorization);

    if (!tokenInfo) {
      throw new UnauthorizedException('Acceso no autorizado');
    }

    return this.categoriaRepositorio.actualizarCategoria(categoria);
  }

}
