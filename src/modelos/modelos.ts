export interface LoginRespuesta {
  loginExitoso: boolean;
  error?: string;
  token?: string;
}

export interface LoginSolicitud {
  email: string;
  password: string;
}

export interface TokenInfoModel {
  usuario_id: number;
  email: string;
  nombre: string;
  rol: string;
  createdAt: number,
}

export interface UsuarioModel {
  id: number;
  usuario_email: string;
  usuario_nombre: string;
  usuario_password: string;
  fecha_creacion: Date;
  creado_por: string;
  activo: boolean;
}

export interface MesaModel {
  id: number;
  numero_mesa: number;
  cantidad_maxima_personas: number;
  mesa_estado_id: number;
  mesa_estado_nombre: string;
  fecha_creacion: Date;
  creado_por: string;
  activo: boolean;
}

export interface MesaEstadoModel {
  id: number;
  mesa_estado_nombre: string;
  fecha_creacion: Date;
  creado_por: string;
  activo: boolean;
}

export interface CategoriaModel {
  id: number;
  activo: boolean;
  creado_por: string;
  fecha_creacion: Date;
  categoria_nombre: string;
}

export interface ProductoModel {
  id: number;
  activo: boolean;
  creado_por: string;
  fecha_creacion: Date;
  precio: number;
  producto_descripcion: string;
  producto_nombre: string;
  producto_tipo_id: string;
  categoria_id: string;
}

export interface ProductoTipoModel {
  id: number;
  activo: boolean;
  creado_por: string;
  fecha_creacion: Date;
  producto_tipo_nombre: string;
}
