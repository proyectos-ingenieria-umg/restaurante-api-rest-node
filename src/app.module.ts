import { Module } from '@nestjs/common';
import { UsuarioController } from './controladores/usuario.controller';
import { UsuarioRepositorio } from './repositorios/usuario.repositorio';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MesaController } from './controladores/mesa.controller';
import { MesaRepositorio } from './repositorios/mesa.repositorio';
import { MesaEstadoRepositorio } from './repositorios/mesa-estado.repositorio';
import { CategoriaController } from './controladores/categoria.controller';
import { CategoriaRepositorio } from './repositorios/categoria.repositorio';
import { ProductoRepositorio } from './repositorios/producto.repositorio';
import { ProductoController } from './controladores/producto.controller';
import { ProductoTipoController } from './controladores/producto-tipo.controller';
import { ProductoTipoRepositorio } from './repositorios/producto-tipo.repositorio';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: '142.93.49.96',
      port: 5432,
      username: 'admin',
      password: 'Admin2020*',
      database: 'proyecto_restaurante',
      entities: [],
      synchronize: true,
      logging: 'all',
      extra: {
        max: 10,
      },
    }),
  ],
  controllers: [
    UsuarioController,
    MesaController,
    CategoriaController,
    ProductoController,
    ProductoTipoController,
  ],
  providers: [
    UsuarioRepositorio,
    MesaEstadoRepositorio,
    MesaRepositorio,
    CategoriaRepositorio,
    ProductoRepositorio,
    ProductoTipoRepositorio,
  ],
})
export class AppModule {
}
